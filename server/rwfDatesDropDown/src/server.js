import express from "express";
import compression from "compression";
import ssr from "./routes/ssr";
import Axios from 'axios';
const app = express();

app.use(compression());
app.use(express.static("public"));

app.use("/firstssr", ssr);

const port = process.env.PORT || 3030;
app.listen(port, function listenHandler() {
  console.info(`Running on ${port}...`);
});

app.get('/matchdates', (req, res) => {
    Axios.get('http://rwfootball.richard-whitehouse.me/v1/match/getMatchDates')
		.then((response) => {
            const { data } = response;
			/*const stylesheet = new ServerStyleSheet();
			const applicationHTML = renderToString(stylesheet.collectStyles(<App data={data} />));
			const css = stylesheet.getStyleTags();
			const fragmentHTML = Fragment(css, Manifest, applicationHTML, data, isDev);*/
			res.send(data);
		})
		.catch((err) => {
			if (err) {
				console.log(err);
				res.status(404).send();
			}
		});
});