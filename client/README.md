# rwFootballWeb

A React project using rwFootballAPI.
Checkout the original basic project for the absolute minimum on setup and how to use.

##Installation#
...
npm install
npm start
...

##API Usage

###Matches

[http://rwfootball.richard-whitehouse.me/v1/match/getMatches](http://rwfootball.richard-whitehouse.me/v1/match/getMatches)
Currently Returns
...
[
    {"matchKey":2,"MatchDate":"20180811","TeamOne_fkey":3,"TeamOneScore":2,"TeamTwo_fkey":4,"TeamTwoScore":2},
    {"matchKey":1,"MatchDate":"20180810","TeamOne_fkey":1,"TeamOneScore":2,"TeamTwo_fkey":2,"TeamTwoScore":1}
]
...


cd C:\\Users\\richy\\Dropbox\\Code\\websites\\RWFootballWeb\\
docker build -t rwfootballweb .
docker-machine ip
docker run -p 4000:80 rwfootballweb

API and React project built by rj_whitehouse2@gmail.com