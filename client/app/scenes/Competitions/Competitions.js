import React, { Component } from 'react'
import ApiCompetitions from 'apiAlias/ApiCompetitions.js';
import ApiLeagueTable from 'apiAlias/ApiLeagueTable.js';

export default class Competitions extends Component {
  constructor(props) {
    super();
    
  }
  render() {
    console.log(this.props.match.params.cmdKey + " is the cmd key");

    let stringData = "";

    console.log("command is " + this.props.match.params.cmdKey);
    if (this.props.match.params.cmdKey == 0 || this.props.match.params.cmdKey == undefined) {
      console.log("command is 0");
      stringData = (
        <ApiCompetitions action = "ListCompetitions" cmdKey={this.props.match.params.cmdKey} />
      );
    } else {
      console.log("command is not 0");
      stringData = (
        <ApiLeagueTable action = "ListLeagueTable" cmdKey={this.props.match.params.cmdKey} />
      );
    }

    return (
      	<div className="matchdate-container">
		  	<div className="title">Competitions</div>
          	{stringData}
      	</div>
    );
  }
}