import React, { Component } from 'react'
import ApiMatchDetail from 'apiAlias/ApiMatchDetail.js';

export default class MatchDetail extends Component {
  constructor(props) {
    super();
    
  }
  render() {
    return (
      <div className="matches-container">
        {<ApiMatchDetail action = "ListMatchDetail" matchKey={this.props.match.params.matchKey} />}
      </div>
    );
  }
}