import React, { Component } from 'react'
import ApiMatches from 'apiAlias/ApiMatches.js';
import MatchFilter from 'componentAlias/Matches/Models/MatchFilter.js';

export default class Matches extends Component {

	updateMatchDates (msg, data) {
		console.log( {data} );
		this.setState({ matchDate: data });
	};

  	constructor(props) {
		super();

		this.state = { 
			matchDate: ""
		}

		console.log("setting props stuff");
		if (this.props && this.props.match != undefined && this.props.match.params.matchDate > 0) {
			console.log("inside if");
			this.setState({ matchDate: this.props.match.params.matchDate });
		}
		console.log("continuing");

		this.updateMatchDates = this.updateMatchDates.bind(this);
		PubSub.subscribe('ListMatchDates', this.updateMatchDates);
	}

	render() {
		return (
			<div className="matches-container">
				{<MatchFilter />}
				{<ApiMatches action = "ListMatches" dateFilter={this.state.matchDate} />}
			</div>
		);
	}
}