import React, { Component } from 'react'
import ApiMatchDates from 'apiAlias/ApiMatchDates.js';

export default class MatchDates extends Component {
  constructor(props) {
    super();
    
  }
  render() {
    return (
      <div className="matchdate-container">
          {<ApiMatchDates action = "ListMatchDates" />}
      </div>
    );
  }
}