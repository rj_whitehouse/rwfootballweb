import React, { Component } from 'react'
import ApiMatchDates from 'apiAlias/ApiMatchDates.js';
import ApiMatches from 'apiAlias/ApiMatches.js';

export default class ScoresAndFixtures extends Component {
  constructor(props) {
    super();
    
  }
  render() {
    return (
      	<div className="matchdate-container">
		  	<div className="title">Scores & Fixtures!</div>
        	{<ApiMatchDates action = "ListMatchDates" />}
		  	{<ApiMatches action = "ListMatches" />}
      	</div>
    );
  }
}