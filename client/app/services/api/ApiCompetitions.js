import React, { Component } from 'react';
import { ajaxDomain } from 'helperAlias/domain.js'
import ListCompetitions from 'componentAlias/Competitions/Models/ListCompetitions.js';

var modelFunction;

export default class ApiCompetition extends Component {
    constructor(props) {
        super();
        this.state = { 
            matches: [],
            fetchInProgress: false
        }
    }

    componentDidMount() {
        this.setState({ fetchInProgress: true });

        var myHeaders = new Headers();
        myHeaders.append('pragma', 'no-cache');
        myHeaders.append('cache-control', 'no-cache');
        
        var myInit = {
          headers: myHeaders
        };

		var urlPath = ajaxDomain();

        // {/* http://competitions.rwfootball.main/v1/competition/getCompetitions */}
        fetch(urlPath + '/v1/competition/getCompetitions/' + (this.props.cmdKey || 0), myHeaders)
        .then(results => {
            if(results.ok) {
              console.log("received competitions");
              return results.json();
            } else {
                throw Error(`Request rejected with status ${results.status}`);
            }
        }).then (data => {
          this.setState({ data })
        })
        .catch(function (error) {
            console.log("looks like an error!");
            console.log(error);
        });
    }
    render() {
        modelFunction = this.props.action;
    
        return (
          <OutputContent data = {this.state.data} />
        )
    }
}

function OutputContent({data}) {
  // can't seem to retrieve function from modelFunction, so set it as a variable outside the component to receive
  console.log("APICompetition OutputContent");
  if (data && Object.keys(data).length > 0) {
      console.log("modelFunction: " + modelFunction);
      var competitions = data;
      if (modelFunction == "ListCompetitions") {
          return (
            <ListCompetitions data = {competitions} />
          )
      } else {
          console.log("Nothing found for " + modelFunction);
      }
  } else {
      return <p>Loading Competitions</p>
  }
}