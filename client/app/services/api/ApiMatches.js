import React, { Component } from 'react';
import { ajaxDomain } from 'helperAlias/domain.js'
import ListMatches from 'componentAlias/Matches/Models/ListMatches.js';

var modelFunction;

export default class ApiMatches extends Component {
    constructor(props) {
        super();
        this.state = { 
            matches: [],
            fetchInProgress: false
        }
    }

    componentDidMount() {

		var dateFilter = this.props.dateFilter;
		console.log("Date Filter: " + dateFilter);

        this.setState({ fetchInProgress: true });

        var myHeaders = new Headers();
        myHeaders.append('pragma', 'no-cache');
        myHeaders.append('cache-control', 'no-cache');
        
        var myInit = {
          headers: myHeaders
        };

		var urlPath = ajaxDomain() + '/v1/match/getMatches';
		
		if (dateFilter != undefined && dateFilter != "") {
			urlPath = urlPath + "?filters=date-" + dateFilter;
		}

        fetch(urlPath, myHeaders)
        .then(results => {
            if(results.ok) {
              console.log("received");
              return results.json();
            } else {
                throw Error(`Request rejected with status ${results.status}`);
            }
        }).then (data => {
          this.setState({ data })
        })
        .catch(function (error) {
            console.log("looks like an error!");
            console.log(error);
        });
    }
    render() {
        modelFunction = this.props.action;
    
        return (
          <OutputContent data = {this.state.data} />
        )
    }
}

function OutputContent({data}) {
  if (data && Object.keys(data).length > 0) {
      var matches = data;
      if (modelFunction == "ListMatches") {
          return (
            <ListMatches data = {matches} />
          )
      } else {
          console.log("Nothing found for " + modelFunction);
      }
  } else {
      return <p>Loading Matches</p>
  }
}