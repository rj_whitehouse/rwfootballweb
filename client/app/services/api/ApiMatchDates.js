import React, { Component } from 'react';
import { ajaxDomain } from 'helperAlias/domain.js'
import ListMatchDates from 'componentAlias/Matches/Models/ListMatchDates.js';
import DropDownMatchDates from 'componentAlias/Matches/Models/DropDownMatchDates.js';

var modelFunction;

export default class ApiMatchDates extends Component {
    constructor(props) {
        super();
        this.state = { 
            matches: [],
            fetchInProgress: false
        }
    }

    componentDidMount() {
        this.setState({ fetchInProgress: true });

        var myHeaders = new Headers();
        myHeaders.append('pragma', 'no-cache');
        myHeaders.append('cache-control', 'no-cache');
        
        /*var myInit = {
          headers: myHeaders
        };*/

        console.log("fetching...");

        var urlPath = ajaxDomain();

        fetch(urlPath + '/v1/match/getMatchDates', myHeaders)
        .then(results => {
            if(results.ok) {
              console.log("received");
              return results.json();
            } else {
                throw Error(`Request rejected with status ${results.status}`);
            }
        }).then (data => {
          this.setState({ data })
        })
        .catch(function (error) {
            console.log("looks like an error in match dates!");
            console.log(error);
        });
    }
    render() {
        modelFunction = this.props.action;
    
        return (
          <OutputContent data = {this.state.data} />
        )
    }
}

function OutputContent({data}) {
  	// can't seem to retrieve function from modelFunction, so set it as a variable outside the component to receive
  	if (data && Object.keys(data).length > 0) {
      	var dates = data;
      	if (modelFunction == "ListMatchDates") {
        	return (
            	<ListMatchDates data = {dates} />
		  	)
		} else if (modelFunction == "DropDownMatchDates") {
			return (
				<DropDownMatchDates data = {dates} />
			)
      	} else {
        	console.log("Nothing found for " + modelFunction);
      	}
  	} else {
    	return <p>Loading Match Dates</p>
  	}
}