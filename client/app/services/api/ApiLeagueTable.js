import React, { Component } from 'react';
import { ajaxDomain } from 'helperAlias/domain.js'
import ListLeagueTable from 'componentAlias/LeagueTable/Models/ListLeagueTable.js';

var modelFunction;

export default class ApiLeagueTable extends Component {
    constructor(props) {
        super();
        this.state = { 
            matches: [],
            fetchInProgress: false
        }
    }

    componentDidMount() {
        this.setState({ fetchInProgress: true });

        var myHeaders = new Headers();
        myHeaders.append('pragma', 'no-cache');
        myHeaders.append('cache-control', 'no-cache');
        
        var myInit = {
          headers: myHeaders
        };

        var urlPath = ajaxDomain();
        fetch(urlPath + '/v1/leaguetable/GetLeagueTables/' + (this.props.cmdKey || 0), myHeaders)
        .then(results => {
            if(results.ok) {
              console.log("received");
              return results.json();
            } else {
                throw Error(`Request rejected with status ${results.status}`);
            }
        }).then (data => {
          this.setState({ data })
        })
        .catch(function (error) {
            console.log("looks like an error!");
            console.log(error);
        });
    }
    render() {
        modelFunction = this.props.action;
    
        return (
          <OutputContent data = {this.state.data} />
        )
    }
}

function OutputContent({data}) {
  // can't seem to retrieve function from modelFunction, so set it as a variable outside the component to receive
  console.log("APILeagueTable OutputContent");
  if (data && Object.keys(data).length > 0) {
      console.log("modelFunction: " + modelFunction);
      var leaguetable = data;
      if (modelFunction == "ListLeagueTable") {
          return (
            <ListLeagueTable data = {leaguetable} />
          )
      } else {
          console.log("Nothing found for " + modelFunction);
      }
  } else {
      return <p>Loading League Table</p>
  }
}