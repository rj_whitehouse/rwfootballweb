export function ajaxDomain () {
	var urlPath = "http://rwfootball.richard-whitehouse.me";
    if (window.location.href.indexOf('localhost')>-1 && process.env.NODE_ENV != "production") {
    	urlPath = "http://gateway.rwfootball.main";
    }
	return urlPath;
}

export function matchDatesAPI() {
	var urlPath = ajaxDomain();

	fetch(urlPath + '/v1/match/getMatchDates', myHeaders)
	.then(results => {
		if(results.ok) {
			console.log("received");
			return results.json();
		} else {
			throw Error(`Request rejected with status ${results.status}`);
		}
	}).then (data => {
		this.setState({ data })
	})
	.catch(function (error) {
		console.log("looks like an error in match dates!");
		console.log(error);
	});
}