import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import 'componentAlias/Matches/Matches.css'

export default class ListMatchDates extends Component {
  constructor(props) {
    super(props);
  }

  render (state) {
    return (
      <div className="matchdate-container">
        <p>Match Date Model</p>

        <OutputContent data = { this.props.data } onSave={this.onSave} />
      </div>
    );
  }
}

class OutputContent extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      data: this.props.data
    };
  }

  render() {
    if (this.state.data && this.state.data.dates && Array.isArray(this.state.data.dates)) {
      let outputData = this.state.data.dates.map((dates) => {
		  
		var stringReturn = "",
			matchDateLink = "/matches/date-" + dates.Date;
        stringReturn = (<div className="dateRow" key={dates.Date}>
          	<div className="dateRow-date">
			  <div className="competitionRowItem competitionRow-competitionName"><Link to={matchDateLink}>{dates.Date}</Link></div>
			</div>
        </div>);
        
        return (stringReturn);
      })
      return outputData;
    } else {
      return <p>Loading Match Dates</p>
    }
  }
}