import React, { Component } from 'react'

export default class ListMatchDates extends Component {
	constructor(props) {
		super(props);
	}

  	change (event){
		console.log("firing event!")
		PubSub.publish('ListMatchDates', event.target.value);
	}

	render (state) {
		return (
			<select onChange={this.change}>
				<OutputContent data = { this.props.data } onSave={this.onSave} />
			</select>
		);
	}
}

class OutputContent extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      data: this.props.data
    };
  }

  render() {
    if (this.state.data && this.state.data.dates && Array.isArray(this.state.data.dates)) {
      let outputData = this.state.data.dates.map((dates) => {
		  
		var stringReturn = (
			<option value={dates.Date}>{dates.Date}</option>
		);
        
        return (stringReturn);
      })
      return outputData;
    } else {
      return <p>Loading Match Dates</p>
    }
  }
}