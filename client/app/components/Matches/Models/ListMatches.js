import React, { Component } from 'react'
import 'componentAlias/Matches/Matches.css'

export default class ListMatches extends Component {
  constructor(props) {
    super(props);
  }

  render (state) {
    return (
      <div className="matches-container">
        <p>Matches Model</p>

        <OutputContent data = { this.props.data } onSave={this.onSave} />
      </div>
    );
  }
}

class OutputContent extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      data: this.props.data
    };
  }

  render() {
    if (this.state.data && this.state.data.matches && Array.isArray(this.state.data.matches)) {
      let outputData = this.state.data.matches.map((mtchs) => {
        var stringReturn = "";
        stringReturn = (<div className="matchRow" key={mtchs.matchKey}>
          <div className="matchRowItem matchRow-matchKey">{mtchs.matchKey}</div>
          <div className="matchRowItem matchRow-date">{mtchs.MatchDate}</div>
          {/*<div className="matchRow-teamOneKey">{mtchs.TeamOne_fkey}</div>*/}
		  <div className="matchRowItem matchRow-teamOneName">{mtchs.TeamOne.TeamName}</div>
          <div className="matchRowItem matchRow-teamOneScore">{mtchs.TeamOneScore}</div>
          <div className="matchRowItem matchRow-Verses">Vs</div>
		  <div className="matchRowItem matchRow-teamTwoName">{mtchs.TeamTwo.TeamName}</div>
          <div className="matchRowItem matchRow-teamTwoScore">{mtchs.TeamTwoScore}</div>
          {/*<div className="matchRow-teamTwoKey">{mtchs.TeamTwo_fkey}</div>*/}
        </div>);
        
        return (stringReturn);
      })
      return outputData;
    } else {
      return <p>Loading Matches</p>
    }
  }
}