import React, { Component } from 'react'
import ApiMatchDates from 'apiAlias/ApiMatchDates.js';

export default class MatchFilter extends Component {
  constructor(props) {
    super(props);
  }

  render (state) {
    return (
      <div className="matchFilter-container">
        <p>Match Filter</p>
		{<ApiMatchDates action = "DropDownMatchDates" />}
      </div>
    );
  }
}