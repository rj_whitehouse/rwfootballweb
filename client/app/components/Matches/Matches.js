import React, { Component } from 'react';
import ApiTransactionData from 'apiAlias/ApiTransactionData.js';

export default class TransactionData extends Component{
  constructor(props) {
    super(props);

    var transactionData = (props) => ApiTransactionData(props);
  }
  render() {
    return (
      <div className="accounts-container">
        Transaction Data
          <ApiTransactionData accountId={this.props.match.params.accountId} />
      </div>
    );
  }
}

