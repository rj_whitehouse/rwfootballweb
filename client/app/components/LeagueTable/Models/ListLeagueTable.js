import React, { Component } from 'react'
import 'componentAlias/LeagueTable/LeagueTable.css'

export default class ListLeagueTable extends Component {
  constructor(props) {
    super(props);
  }

  render (state) {
    return (
      <div className="leagueTable-container">
        <p>League Table Model</p>

        <OutputContent data = { this.props.data } onSave={this.onSave} />
      </div>
    );
  }
}

class OutputContent extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      data: this.props.data
    };
  }

  render() {
    if (this.state.data && this.state.data.LeagueData && Array.isArray(this.state.data.LeagueData)) {

      let tableData = this.state.data.LeagueData.map((leagueData) => {
        var stringReturn = "";
        stringReturn = (<div className="leagueTableRow" key={leagueData.TeamKey}>
          <div className="leagueTableRowItem leagueTableRow-teamName">{leagueData.TeamName}</div>
          <div className="leagueTableRowItem leagueTableRow-points">{leagueData.Played}</div>
          <div className="leagueTableRowItem leagueTableRow-won">{leagueData.Won}</div>
          <div className="leagueTableRowItem leagueTableRow-drawn">{leagueData.Drawn}</div>
          <div className="leagueTableRowItem leagueTableRow-lost">{leagueData.Lost}</div>
    		  <div className="leagueTableRowItem leagueTableRow-points">{leagueData.Points}</div>
        </div>);
        
        return (stringReturn);
      })


      let outputData = (<div className="leagueTableBlock">
        <div className="leagueTableBlockItem">{this.state.data.Date}</div>
        <div className="leagueTableBlockItem">{tableData}</div>
      </div>)
      
      return outputData;
    } else {
      return <p>Loading League Table</p>
    }
  }
}