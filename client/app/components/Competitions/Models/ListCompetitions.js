import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import 'componentAlias/Competitions/Competitions.css'

export default class ListCompetitions extends Component {
  constructor(props) {
    super(props);
  }

  render (state) {
    return (
      <div className="competition-container">
        <p>Competitions Model</p>

        <OutputContent data = { this.props.data } onSave={this.onSave} />
      </div>
    );
  }
}

class OutputContent extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      data: this.props.data
    };
  }

  render() {
    if (this.state.data && this.state.data.competitions && Array.isArray(this.state.data.competitions)) {
      let competitions = this.state.data.competitions.map((leagueData) => {
        var stringReturn = "",
          competitionLink = "competitions/" + leagueData.competitionKey;
        stringReturn = (
          <div className="competitionRow" key={leagueData.competitionKey}>
            <div className="competitionRowItem competitionRow-competitionName">{leagueData.CompetitionName}</div>
            <div className="competitionRowItem competitionRow-competitionName"><Link to={competitionLink}>League Table</Link></div>
          </div>
        );
        
        return (stringReturn);
      })
      
      return competitions;
    } else {
      return <p>Loading Competitions</p>
    }
  }
}