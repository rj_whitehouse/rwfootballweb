import React from 'react'
import { Link } from 'react-router-dom'

import HeaderLogo from 'componentAlias/Header/HeaderLogo/HeaderLogo.js'
import 'componentAlias/Header/Header.css'

// The Header creates links that can be used to navigate
// between routes.
const Header = () => (
	<header>
		<div className="headerBlock">
			<HeaderLogo />
		</div>
		<div className="headerBlock">
			<nav>
				<ul>
					<li><Link to='/'>Home</Link></li>
					<li><Link to='/matches'>Matches</Link></li>
					<li><Link to='/dates'>Dates</Link></li>
					<li><Link to='/scores-fixtures'>Scores &amp; Fixtures</Link></li>
					<li><Link to='/competitions'>Competitions</Link></li>
				</ul>
			</nav>
			{process.env.NODE_ENV}
		</div>
	</header>
)

export default Header