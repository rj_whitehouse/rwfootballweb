import React, { Component } from 'react'
import Header from './Header'
import Main from './Main'
import PubSub from 'pubsub-js';

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render (state) {
    return (<div>
      <Header />
      <Main />
    </div>)
  }
}