import React from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import Home from './Home'
import Matches from 'sceneAlias/Matches/MatchData.js'
import MatchDetail from 'sceneAlias/Matches/MatchDetail.js'
import MatchDates from 'sceneAlias/Matches/MatchDates.js'
import ScoresAndFixtures from 'sceneAlias/Matches/ScoresAndFixtures.js'
import Competitions from 'sceneAlias/Competitions/Competitions.js'

const Main = (match) => (
  <main>
    <Switch>
      	<Route exact path='/' component={Home}/>
      	<Route exact path='/matches' component={Matches}/>
		<Route exact path={`/matches/date-:matchDate`} component={Matches}/>
      	<Route path={`/matches/:matchKey`} component={MatchDetail}/>
		<Route exact path='/dates' component={MatchDates}/>
	    <Route exact path='/scores-fixtures' component={ScoresAndFixtures}/>
      	<Route exact path='/competitions' component={Competitions}/>
      	<Route exact path='/competitions/:cmdKey' component={Competitions}/>
    </Switch>
  </main>
)

export default Main
