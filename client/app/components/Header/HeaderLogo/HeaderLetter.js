import React from 'react';
import 'componentAlias/Header/HeaderLogo/HeaderLetter.css'

const HeaderLetter = props => {
	return (<div className="headerLetter">{props.data}</div>)
}

export default HeaderLetter
