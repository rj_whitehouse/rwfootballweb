import React from 'react';
import HeaderLetter from 'componentAlias/Header/HeaderLogo/HeaderLetter.js'

const HeaderLogo = () => (
	<div className="headerLogo">
		<HeaderLetter data = {"R"} />
		<HeaderLetter data = {"W"} />
		<HeaderLetter data = {"F"} />
	</div>
)

export default HeaderLogo
