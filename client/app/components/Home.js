import React, { Component } from 'react';

export default class Home extends React.Component {
  
  render() {
    return (
      <div className="app-container">
        <p>Home Content Stuff!</p>
      </div>
    );
  }
}