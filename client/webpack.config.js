path = require('path');

var HTMLWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin(
    {
        template: __dirname + '/app/index.html',
        filename: 'index.html',
        inject: 'body'
    }
);

const rootDirectory = path.resolve(__dirname, '../');
const appDirectory = path.resolve(__dirname, '../app');

const babelLoaderConfiguration = {
    test: /\.js$/,
    // Add every directory that needs to be compiled by Babel during the build.
    include: [
      path.resolve(rootDirectory, 'node_modules/react-native-uncompiled'),
      path.resolve(rootDirectory, 'node_modules/react-native-vector-icons'),
      path.resolve(rootDirectory, 'node_modules/react-navigation'),
      path.resolve(rootDirectory, 'node_modules/react-native-drawer-layout'),
      path.resolve(rootDirectory, 'node_modules/react-native-dismiss-keyboard'),
      path.resolve(rootDirectory, 'node_modules/react-native-safe-area-view'),
      path.resolve(rootDirectory, 'node_modules/react-native-tab-view'),
      path.resolve(appDirectory, 'node_modules/react-native-implementation'),
    ],
    use: {
      loader: 'babel-loader',
      options: {
        cacheDirectory: true,
        // Babel configuration (or use .babelrc)
        // This aliases 'react-native' to 'react-native-web' and includes only
        // the modules needed by the app.
        plugins: [
            // This is needed to polyfill ES6 async code in some of the above modules
            'babel-polyfill',
            // This aliases 'react-native' to 'react-native-web' to fool modules that only know
            // about the former into some kind of compatibility. 
            'react-native-web'
            ],
        // The 'react-native' preset is recommended to match React Native's packager
        presets: ['react-native']
      }
    }
};

module.exports = {
    entry: __dirname + '/app/index.js',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            babelLoaderConfiguration
        ]
    },
    mode: "production",
    target: "web",
    output: {
        filename: 'transformed.js',
        path: __dirname + '/build',
        publicPath: '/'
	},
	performance: { 
		maxEntrypointSize: 512000,
		maxAssetSize: 512000 
	},
    plugins: [HTMLWebpackPluginConfig],
    devServer: {
        historyApiFallback: true,
    },
    resolve: {
        // root: [path.resolve('./src')],
        // modules: [path.resolve('./app')],
        extensions: ['*', '.js', '.jsx'],
        alias: {
            apiAlias: path.resolve(__dirname, './app/services/api/'),
			componentAlias: path.resolve(__dirname, './app/components/'),
			helperAlias: path.resolve(__dirname, './app/helpers/'),
            sceneAlias: path.resolve(__dirname, './app/scenes/')
        },
    }
};

